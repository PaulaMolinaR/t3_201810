package model.data_structures;

import java.util.Iterator;

public interface IQueue<E> {
	
	/** Enqueue a new element at the end of the queue */
	public void enqueue(E item);
	
	/** Dequeue the "first" element in the queue
	 * @return "first" element or null if it doesn't exist
	 */
	public E dequeue();
	
	/** Evaluate if the queue is empty. 
	 * @return true if the queue is empty. false in other case.
	 */
	public boolean isEmpty();
	
	/**
	 * Numeros de elementos en la lista
	 */
	public int size();
	
	/**
     * Returns an iterator that iterates over the items in this queue in FIFO order.
     *
     * @return an iterator that iterates over the items in this queue in FIFO order
     */
    public Iterator<E> iterator(); 
	
}
