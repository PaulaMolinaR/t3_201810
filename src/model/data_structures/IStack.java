package model.data_structures;

import java.util.Iterator;

public interface IStack<E> {

	/** Push a new element at the top of the stack */
	public void push (E item);
	
	/** Pop the element at the top of the stack 
	 * @return the top element or null if it doesn't exist
	 * */
	public E pop();
	
	/** Evaluate if the stack is empty
	 * @return true if the stack is empty. false in other case. 
	 */
	public boolean isEmpty();
	
	/**
	 * Numeros de elementos en la lista
	 */
	public int size();
	
	/**
     * Returns an iterator to this stack that iterates through the items in LIFO order.
     *
     * @return an iterator to this stack that iterates through the items in LIFO order
     */
	public Iterator<E> iterator();
}
