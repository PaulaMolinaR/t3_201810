package model.logic;

import java.io.BufferedReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Service;

public class TaxiTripsManager implements ITaxiTripsManager 
{

	// Definition of data model 
	private IStack<Service> taxiServicesStack= new Stack<Service>();
	private IQueue<Service> taxiServicesQueue=new Queue<Service>();

	public void loadServices(String serviceFile, String taxiId) 
	{

		try
		{
			BufferedReader reader = new BufferedReader(new FileReader( serviceFile ));
			Gson gson = new GsonBuilder().create();

			Service[] taxiServices = gson.fromJson(reader, Service[].class);

			for(int i = 0; i < taxiServices.length; i++)
			{
				Service taxiService = (Service) taxiServices[i];

				if(taxiService.getTaxi_id().equals(taxiId))
				{
					taxiServicesStack.push(taxiService);
					taxiServicesQueue.enqueue(taxiService);
				}

			}

		}   
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();

		}		


		System.out.println("Inside loadServices with File:" + serviceFile);
		System.out.println("Inside loadServices with TaxiId:" + taxiId);

	}

	@Override
	public int[] servicesInInverseOrder() 
	{	
		//Lo que esta en verde es para revisar que el metodo si es correcto
		//ArrayList<String> servicios = new ArrayList<String>();
		int[] resultado = new int[2];
		int servicesInInverseOrder = 1;
		int servicesNotInInverseOrder = 1;
		if(!taxiServicesStack.isEmpty())
		{
			Service service1 = taxiServicesStack.pop();
			Service service2 = taxiServicesStack.pop();

			//Lo que esta en verde es para revisar que el metodo si es correcto
			//servicios.add(service1.getTrip_start_timestamp());

			while(taxiServicesStack.isEmpty() == false)
			{	
				if(service1.getTrip_start_timestamp().compareTo(service2.getTrip_start_timestamp()) == 1)
				{
					//servicios.add(service2.getTrip_start_timestamp());
					servicesInInverseOrder++;
					service1 = service2;
					service2 = taxiServicesStack.pop();

				}
				else
				{
					servicesNotInInverseOrder++;
					service2 = taxiServicesStack.pop();
				}
			}

			resultado[0] = servicesInInverseOrder;
			resultado[1] = servicesNotInInverseOrder;

		}
		else
		{

			System.out.println("ERROR!! the stack is empty, please load some services");
		}



		//for(int i = 0; i < servicios.size(); i++)
		//{
		//String string = servicios.get(i);
		//System.out.println(string);
		//}

		return resultado;
	}

	@Override
	public int[] servicesInOrder() 
	{
		//ArrayList<String> servicios = new ArrayList<String>();
		int [] resultado = new int[2];
		int servicesInOrder = 1;
		int servicesNotInOrder = 1;
		if(!taxiServicesQueue.isEmpty())
		{
			Service service1 = taxiServicesQueue.dequeue();
			Service service2 = taxiServicesQueue.dequeue();

			//servicios.add(service1.getTrip_start_timestamp());

			while(taxiServicesQueue.isEmpty() == false)
			{
				if(service1.getTrip_start_timestamp().compareTo(service2.getTrip_start_timestamp()) == -1)
				{
					//servicios.add(service2.getTrip_start_timestamp());
					servicesInOrder++;
					service1 = service2;
					service2 = taxiServicesQueue.dequeue();	
				}
				else
				{
					servicesNotInOrder++;
					service2 = taxiServicesQueue.dequeue();
				}
			}

			resultado[0] = servicesInOrder;
			resultado[1] = servicesNotInOrder;	
		}
		else
		{
			System.out.println("ERROR!! the queue is empty, please load some services");

		}


		//for(int i = 0; i < servicios.size(); i++)
		//{
		//String string = servicios.get(i);
		//System.out.println(string);
		//}


		return resultado;
	}


}
