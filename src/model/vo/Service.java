package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> 
{
	private String trip_id;
	private String taxi_id;
	private String trip_seconds; 
	private String trip_miles; 
	private String trip_total;
	private String trip_start_timestamp;
	

	public String getTrip_id() {
		return trip_id;
	}


	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}


	public String getTaxi_id() {
		return taxi_id;
	}


	public void setTaxi_id(String taxi_id) {
		this.taxi_id = taxi_id;
	}

	
	public String getTrip_seconds() {
		return trip_seconds;
	}


	public void setTrip_seconds(String trip_seconds) {
		this.trip_seconds = trip_seconds;
	}


	public String getTrip_miles() {
		return trip_miles;
	}


	public void setTrip_miles(String trip_miles) {
		this.trip_miles = trip_miles;
	}


	public String getTrip_total() {
		return trip_total;
	}


	public void setTrip_total(String trip_total) {
		this.trip_total = trip_total;
	}


	public String getTrip_start_timestamp() {
		return trip_start_timestamp;
	}

	public void setTrip_start_timestamp(String trip_start_timestamp) {
		this.trip_start_timestamp = trip_start_timestamp;
	}


	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
