package model.data_structures.test;

import junit.framework.TestCase;
import model.data_structures.IQueue;
import model.data_structures.Queue;

public class QueueTest extends TestCase
{
	//------------------------------------------------
	//Atributes
	//------------------------------------------------
		
	private IQueue<String> queue;
		
	//------------------------------------------------
	//Methods
	//------------------------------------------------
			
	/**
	 * Escenario 1: Crea una lista vacía.
	 * @throws Exception 
	 */
		
	public void setupEscenario1() throws Exception 
	{
		try 
		{
			queue = new Queue<String>();
		} 
		catch ( Exception e) 
		{
			throw new Exception("No se debería generar el error " + e.getMessage());
		}
	}
			
	/**
	 * Escenario 2: Crea una lista con elementos.
	 * @throws Exception 	
	 */
	public void setupEscenario2() throws Exception 
	{
		try
		{
			queue = new Queue<String>();
			
			String manzana = "Manzana";
			String pera = "Pera";
			String naranja = "Naranja";
			String uva = "Uva";
			String limon = "Limon";
	
			queue.enqueue(manzana);
			queue.enqueue(pera);
			queue.enqueue(naranja);
			queue.enqueue(uva);
			queue.enqueue(limon);
		}
		catch(Exception e)
		{
			throw new Exception("No se debería generar el error " + e.getMessage());
		}
		}
		
		public void testIsEmpty() throws Exception
		{
			// 1
			setupEscenario1();
			assertEquals("El tamaño no corresponde.", true, queue.isEmpty());
				
			// 2
			setupEscenario2();
			assertEquals("El tamaño no corresponde.", false, queue.isEmpty());	
		}
		
		public void testSize() throws Exception
		{
			// 1
			setupEscenario1();
			assertEquals("El tamaño no corresponde.", 0, queue.size());
		
			// 2
			setupEscenario2();
			assertEquals("El tamaño no corresponde.", 5, queue.size());			
		}
		
		public void testEnqueue() throws Exception
		{
			String mandarina = "mandarina";
			
			// 1
			setupEscenario1();
			queue.enqueue(mandarina);
			assertEquals("No se agrego el elemento", 1, queue.size());
				
			// 2
			setupEscenario2();
			queue.enqueue(mandarina);
			assertEquals("No se agrego el elemento", 6, queue.size());
		}
		
		public void testDequeue() throws Exception
		{		
			// 2
			setupEscenario2();
			assertEquals("No es el elemento esperado", "Manzana", queue.dequeue());
		}
}
