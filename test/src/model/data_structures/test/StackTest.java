package model.data_structures.test;

import junit.framework.TestCase;
import model.data_structures.IStack;
import model.data_structures.Stack;

public class StackTest extends TestCase
{
	//------------------------------------------------
	//Atributes
	//------------------------------------------------
	
	private IStack<String> stack;
	
	//------------------------------------------------
	//Methods
	//------------------------------------------------
		
	/**
	 * Escenario 1: Crea una lista vacía.
	 * @throws Exception 
	 */
	
	public void setupEscenario1() throws Exception 
	{
		try 
		{
			stack = new Stack<String>();
		} 
		catch ( Exception e) 
		{
			throw new Exception("No se debería generar el error " + e.getMessage());
		}
	}
		
	/**
	 * Escenario 2: Crea una lista con elementos.
	 * @throws Exception 	
	 */
	public void setupEscenario2() throws Exception 
	{
		try
		{
			stack = new Stack<String>();
			
			String manzana = "Manzana";
			String pera = "Pera";
			String naranja = "Naranja";
			String uva = "Uva";
			String limon = "Limon";
	
			stack.push(manzana);
			stack.push(pera);
			stack.push(naranja);
			stack.push(uva);
			stack.push(limon);
		}
		catch(Exception e)
		{
			throw new Exception("No se debería generar el error " + e.getMessage());
		}
	}
	
	public void testIsEmpty() throws Exception
	{
		// 1
		setupEscenario1();
		assertEquals("El tamaño no corresponde.", true, stack.isEmpty());
			
		// 2
		setupEscenario2();
		assertEquals("El tamaño no corresponde.", false, stack.isEmpty());	
	}
	
	public void testSize() throws Exception
	{
		// 1
		setupEscenario1();
		assertEquals("El tamaño no corresponde.", 0, stack.size());
	
		// 2
		setupEscenario2();
		assertEquals("El tamaño no corresponde.", 5, stack.size());			
	}
	
	public void testPush() throws Exception
	{
		String mandarina = "mandarina";
		
		// 1
		setupEscenario1();
		stack.push(mandarina);
		assertEquals("No se agrego el elemento", 1, stack.size());
			
		// 2
		setupEscenario2();
		stack.push(mandarina);
		assertEquals("No se agrego el elemento", 6, stack.size());
	}
	
	public void testPop() throws Exception
	{		
		// 2
		setupEscenario2();
		assertEquals("No es el elemento esperado", "Limon", stack.pop());
	}
	
}
